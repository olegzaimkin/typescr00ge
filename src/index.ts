import ts from 'typescript';
var fs = require('fs');
var path = require('path');

const rootPath = 'C:/projects-work/ar-wd-core/src/persistence/';
const filename = 'docmodel.ts';
// const rootPath = './';
// const filename = 'sample-data/model.ts';

const resultFileName = 'sample-data/rdlmodel.ts';
const rootNames = [filename]
	.map(file => path.resolve(rootPath, file));

const program = ts.createProgram(rootNames, {
	rootDir: rootPath,
	sourceRoot: rootPath,
});
console.log(program.getRootFileNames(), path.resolve(rootPath, filename));

const sourceFile = program.getSourceFile(rootPath + filename);
const typeChecker = program.getTypeChecker();

function printRecursiveFrom(
	node: ts.Node, indentLevel: number, sourceFile: ts.SourceFile
) {
	const indentation = "..".repeat(indentLevel);
	const syntaxKind = ts.SyntaxKind[node.kind];
	// const nodeText = node.getText(sourceFile);
	const nodeText = printer.printNode(ts.EmitHint.Unspecified, node as ts.TypeNode, sourceFile);
	console.log(`${indentation}${syntaxKind}: ${nodeText}`);

	ts.forEachChild(node, child =>
		printRecursiveFrom(child, indentLevel + 1, sourceFile)
	);
}

const printer = ts.createPrinter({ newLine: ts.NewLineKind.CarriageReturnLineFeed, removeComments: false })
const writeStream = fs.createWriteStream(resultFileName);

/*********************************************/

/** Flattens (and reduces at the same time) type definition. */
function expandTypeDecl(node: ts.Node): ts.TypeNode {
	var type = typeChecker.getTypeAtLocation(node);

	// back to node
	var rnode = typeChecker.typeToTypeNode(type, undefined,
		ts.NodeBuilderFlags.NoTruncation
		| ts.NodeBuilderFlags.InTypeAlias
		);
	return rnode!;
}

type TypeDeclNode = {
	Name: string;
	Decl: ts.TypeAliasDeclaration;
	Type: ts.TypeNode;
	TypeDecl: string
}

function decl(type: ts.TypeNode) {
	return printer.printNode(ts.EmitHint.Unspecified, type, sourceFile!);
}

ts.createPrinter()

function printType(node: ts.TypeNode): string {
	return printer.printNode(ts.EmitHint.Unspecified, node, sourceFile!);
	var type = typeChecker.getTypeAtLocation(node);
	return typeChecker.typeToString(type, undefined,
		ts.TypeFormatFlags.NoTruncation
		| ts.TypeFormatFlags.MultilineObjectLiterals
		| ts.TypeFormatFlags.InTypeAlias
		| ts.TypeFormatFlags.UseSingleQuotesForStringLiteralType
		// | ts.TypeFormatFlags.UseAliasDefinedOutsideCurrentScope	// prevents type expansion
		// | ts.TypeFormatFlags.UseStructuralFallback
		);
}

if (sourceFile) {
	// recursivelyPrintVariableDeclarations(sourceFile, sourceFile);
	console.log('********************');
	const typeDecls: TypeDeclNode[] = [];

	ts.forEachChild(sourceFile, child => {
		if (ts.isTypeAliasDeclaration(child))  {
			const typeName = child.name.text;
			const type = expandTypeDecl(child.type);
			if (child.typeParameters == undefined) typeDecls.push({ Name: typeName, Decl: child, Type: type, TypeDecl: decl(type) });
		}
	});

	const reduced = reduceTypes(typeDecls);
	reduced.forEach(decl => {
		const typeDecl = printType(decl.Type);
		writeStream.write(`export type ${decl.Name} = ${typeDecl};\n`);
	});

	// compareTypeSample(typeDecls);
	// modifyTypeSample(typeDecls);
}


function createReportItemsType() {
	return ts.factory.createArrayTypeNode(ts.factory.createTypeReferenceNode(ts.factory.createIdentifier('ReportItem')));
}

function isReportItemsNode(node: ts.TypeNode) {
	return ts.isArrayTypeNode(node)
		&& ts.isTypeReferenceNode(node.elementType)
		&& ts.isIdentifier(node.elementType.typeName)
		&& node.elementType.typeName.text === "ReportItem";
}

/** checks if any of the type literal elements can be reduced to any type in the typeDecls */
function reduceType(type: ts.TypeNode, typeDecls: TypeDeclNode[], ctx: string = '', level: number = 0): ts.TypeNode {
	// if (ctx.indexOf('ReportItem/0') >= 0)
	// 	console.log(". ".repeat(level), ctx, ts.SyntaxKind[type.kind]);
	if (ts.isArrayTypeNode(type)) {
		const reducedType = reduceType(type.elementType, typeDecls, `${ctx}.[]`, level + 1);
		if(reducedType !== type.elementType) {
			return ts.factory.createArrayTypeNode(reducedType);
		}
	}

	if (level > 0) {
		const lookupValue = decl(type);
		// if (ctx === 'ReportItem/0') console.log(`report item lookup\n${lookupValue}`);
		for (let i = 0; i < typeDecls.length; i++) {
			// if(ctx === 'ReportItem/0' && typeDecls[i].Name === 'Textbox') console.log(typeDecls[i].TypeDecl);
			if (typeDecls[i].TypeDecl === lookupValue) {
				return ts.factory.createTypeReferenceNode(typeDecls[i].Name);
			}
		}
	}
	if(ts.isTypeReferenceNode(type) && type.typeArguments == null) {
		const { typeName } = type;
		if(ts.isIdentifier(typeName) && typeName.text === 'RdlReportItemBase') {
			const newName = ts.factory.createIdentifier('ReportItem');
			return ts.factory.updateTypeReferenceNode(type, newName, undefined);
		}
	}
	else if(ts.isIntersectionTypeNode(type)) {
		const newTypes = [...type.types];
		var updateCount = 0;
		for(let mi = 0; mi < newTypes.length; mi++) {
			const reducedType = reduceType(newTypes[mi], typeDecls, `${ctx}/${mi}`, level + 1);
			if (type.types[mi] !== reducedType) {
				newTypes[mi] = reducedType;
				updateCount++;
			}
		}
		if (updateCount > 0) return ts.factory.updateIntersectionTypeNode(type, ts.factory.createNodeArray(newTypes));
	}
	// commented out as we don't need it and
	else if(ts.isUnionTypeNode(type) && !ts.isLiteralTypeNode(type.types[0])) {
		const newTypes = [...type.types];
		var updateCount = 0;
		for(let mi = 0; mi < newTypes.length; mi++) {
			const reducedType = reduceType(newTypes[mi], typeDecls, `${ctx}/${mi}`, level + 1);
			if (type.types[mi] !== reducedType) {
				newTypes[mi] = reducedType;
				updateCount++;
			}
		}
		if (updateCount > 0) return ts.factory.updateUnionTypeNode(type, ts.factory.createNodeArray(newTypes));
	}
	else if(ts.isParenthesizedTypeNode(type)) {
		const reducedType = reduceType(type.type, typeDecls, `(${ctx})`, level + 1);
		if (type.type !== reducedType) {
			return ts.factory.updateParenthesizedType(type, reducedType);
		}
	}
	else if(ts.isTypeReferenceNode(type) && type.typeArguments != null) {
		const newArgs = [...type.typeArguments];
		var updateCount = 0;
		for(let mi = 0; mi < newArgs.length; mi++) {
			const reducedType = reduceType(newArgs[mi], typeDecls, `${ctx}<${mi}>`, level + 1);
			if (newArgs[mi] !== reducedType) {
				newArgs[mi] = reducedType;
				updateCount++;
			}
		}
		if (updateCount > 0) return ts.factory.updateTypeReferenceNode(type, type.typeName, ts.factory.createNodeArray(newArgs));
	}
	else if (ts.isTypeLiteralNode(type)) {
		const newMembers: (ts.TypeElement | null)[] = [...type.members];
		var updateCount = 0;
	
		for(let i = 0; i < newMembers.length; i++) {
			const member = newMembers[i] as ts.PropertySignature;
			const memberName = member.name && ts.isIdentifier(member.name) && member.name.text || '';
			let reducedType: ts.TypeNode | null = member.type!;

			// whitewash dummy fields
			if (member.type?.kind === ts.SyntaxKind.UndefinedKeyword || member.type?.kind === ts.SyntaxKind.NeverKeyword) {
				reducedType = null;
			}
			// fixup { Children?: WithRecChildrenMaybe... } declaration
			else if(memberName === 'Children' && member.type != null
				&& ts.isArrayTypeNode(member.type)
				&& ts.isTypeReferenceNode(member.type.elementType)
				&& ts.isIdentifier(member.type.elementType.typeName) && member.type.elementType.typeName.text === 'WithRecChildrenMaybe'
			) {
				reducedType = ts.factory.createArrayTypeNode(ts.factory.createTypeReferenceNode(ts.factory.createIdentifier('TablixMember')))
			}
			// replace any generic report itesms references to a single ReportItems type
			else if (memberName == 'ReportItems' && !isReportItemsNode(member.type!)) {
				reducedType = createReportItemsType();
			} else {
				reducedType = reduceType(member.type!, typeDecls, `${ctx}.${memberName}`, level + 1);
			}

			if(reducedType !== member.type) {
				newMembers[i] = reducedType && ts.factory.updatePropertySignature(member, member.modifiers, member.name, member.questionToken, reducedType);
				updateCount++;
			}
		}	
		if (updateCount > 0) {
			const members: ts.TypeElement[] = newMembers.filter(m => m != null) as any;
			return ts.factory.updateTypeLiteralNode(type, ts.factory.createNodeArray(members));
		}
	}

	return type;
}

function reduceTypes(typeDecls: TypeDeclNode[]): TypeDeclNode[] {
	const result = [...typeDecls];

	let extra = 2;
	let totalReductionCount = 0;
	let updateCount = 0;
	do {
		updateCount = 0;
		for(let i = 0; i < result.length; i++) {
			const type = result[i].Type;
			const updated = reduceType(type, typeDecls, result[i].Name);
			if (updated !== type) {
				updateCount++;
				result[i] = { ...result[i], Type: updated, TypeDecl: decl(updated) };
			}
		}
		typeDecls = result; // reapply
		totalReductionCount += updateCount;
	} while(updateCount > 0 || extra-- > 0);

	console.info(`reduceTypes completed with ${totalReductionCount} types processed`);

	return result;
}

writeStream.end();
