export type BorderStyleEnum = "None" | "Solid";
export type BorderStyle = { Color?: string; Style?: BorderStyleEnum; Width?: string; };
export type BorderSideStyle = { Color?: string; Style?: BorderSideStyleEnum; Width?: string; };
export type BorderSideStyleEnum = "" | "None" | "Solid";
export type Style = { Border: BorderStyle; TopBorder: BorderSideStyle; LeftBorder: BorderSideStyle; BottomBorder: BorderSideStyle; RightBorder: BorderSideStyle; };
export type StyleExp = Style;
export type Item = { type: "item"; name: string; style: Style };
