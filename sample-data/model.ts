export type BorderStyleEnum = 'None' | 'Solid';

export type BorderStyle = {
	Color?: string;
	Style?: BorderStyleEnum;
	Width?: string;
};

export type BorderSideStyle = {
	Color?: string;
	Style?: '' | 'None' | 'Solid';
	Width?: string;
};

export type BorderSideStyleEnum = NonNullable<BorderSideStyle['Style']>;

export type Style = {
    Border: BorderStyle;
    TopBorder: BorderSideStyle;
    LeftBorder: BorderSideStyle;
    BottomBorder: BorderSideStyle;
    RightBorder: BorderSideStyle;
};

export type StyleExp = ExpandRecursively<Style>;

function item() : {
    /** type of the element */
    type: 'item',
    name: string,
    style: Style,
    style1: {
        LineStyle: BorderSideStyle;
    }
} {

    const value = 'abcd';
    return { type: 'item', name: value, style: {} as any, style1: {} as any };
}

export type Expand<T> = T extends infer O ? { [K in keyof O]: O[K] } : never;
export type ExpandRecursively<T> = T extends object
  ? T extends infer O ? { [K in keyof O]: ExpandRecursively<O[K]> } : never
  : T;

export type Item = ExpandRecursively<ReturnType<typeof item>>;
