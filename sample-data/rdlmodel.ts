export type Filter = {
  FilterExpression: string;
  Operator: FilterOperator;
  FilterValues: string[];
};
export type FilterOperator =
  | "Between"
  | "Equal"
  | "Like"
  | "NotEqual"
  | "GreaterThan"
  | "GreaterThanOrEqual"
  | "LessThan"
  | "LessThanOrEqual"
  | "TopN"
  | "BottomN"
  | "TopPercent"
  | "BottomPercent"
  | "In";
export type ThreeStateBoolean = "False" | "Auto" | "True";
export type DataElementOutput = "Auto" | "Output" | "NoOutput" | "ContentsOnly";
export type BorderStyle = {
  Color?: string;
  Style?:
    | "Default"
    | "None"
    | "Dotted"
    | "Dashed"
    | "Solid"
    | "Double"
    | "DashDot"
    | "DashDotDot"
    | "Groove"
    | "Ridge"
    | "Inset"
    | "WindowInset"
    | "Outset";
  Width?: string;
};
export type SideStyle = {
  Color?: string;
  Style?:
    | ""
    | "Default"
    | "None"
    | "Dotted"
    | "Dashed"
    | "Solid"
    | "Double"
    | "DashDot"
    | "DashDotDot"
    | "Groove"
    | "Ridge"
    | "Inset"
    | "WindowInset"
    | "Outset";
  Width?: string;
};
export type BackgroundImage = {
  Source?: "External" | "Embedded" | "Database";
  Value?: string;
  MIMEType?: string;
  BackgroundRepeat?: string;
};
export type Visibility = { Hidden?: string; ToggleItem?: string };
export type Action = {
  Hyperlink?: string;
  Drillthrough?: {
    ReportName?: string;
    Parameters?: { ParameterName: string; Value?: string; Omit?: string }[];
  };
  BookmarkLink?: string;
};
export type UserSort = {
  SortExpression: string;
  SortExpressionScope?: string;
  SortTarget?: string;
};
export type Grouping = {
  Name?: string;
  DocumentMapLabel?: string;
  GroupExpressions?: string[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  PageBreakDisabled?: string;
  Filters?: Filter[];
  ParentGroup?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  DataCollectionName?: string;
  NewSection?: boolean;
};
export type SortExpression = {
  Value?: string;
  Direction?: "Ascending" | "Descending";
};
export type Textbox = {
  Name: string;
  Value?: string;
  Type: "textbox";
  Action?: Action;
  CanGrow?: boolean;
  CanShrink?: boolean;
  DataElementStyle?: "Auto" | "AttributeNormal" | "ElementNormal";
  KeepTogether?: boolean;
  ToggleImage?: { InitialState: string };
  UserSort?: UserSort;
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    LineHeight?: string;
    LineSpacing?: string;
    CharacterSpacing?: string;
    WrapMode?: string;
    HeadingLevel?: string;
    ShrinkToFit?: string;
    TextJustify?: "Auto" | "Distribute" | "DistributeAllLines";
    Angle?: string;
    MinCondenseRate?: string;
    Format?: string;
    TextDecoration?: string;
    TextAlign?: string;
    VerticalAlign?: string;
    Color?: string;
    Direction?: string;
    WritingMode?: string;
    Language?: string;
    Calendar?: string;
    NumeralLanguage?: string;
    NumeralVariant?: string;
    UnicodeBiDi?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type Checkbox = {
  Name: string;
  Type: "checkbox";
  DataElementStyle?: "Auto" | "AttributeNormal" | "ElementNormal";
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    TextDecoration?: string;
    WrapMode?: string;
    Color?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  CheckAlignment?:
    | "MiddleLeft"
    | "TopLeft"
    | "TopCenter"
    | "TopRight"
    | "MiddleCenter"
    | "MiddleRight"
    | "BottomLeft"
    | "BottomCenter"
    | "BottomRight";
  Text?: string;
  Checked?: string;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type List = {
  Name: string;
  Type: "list";
  KeepTogether?: boolean;
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    LineHeight?: string;
    Format?: string;
    TextDecoration?: string;
    TextAlign?: string;
    VerticalAlign?: string;
    Color?: string;
    Direction?: string;
    WritingMode?: string;
    Language?: string;
    Calendar?: string;
    NumeralLanguage?: string;
    NumeralVariant?: string;
    UnicodeBiDi?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  Filters?: Filter[];
  NewSection?: boolean;
  SortExpressions?: SortExpression[];
  ConsumeWhiteSpace?: boolean;
  DataInstanceName?: string;
  DataInstanceElementOutput?: "Output" | "NoOutput";
  Group?: Grouping;
  ReportItems?: ReportItem[];
  NoRowsMessage?: string;
  DataSetName?: string;
  DataSetParameters?: { [x: string]: any; Name: string; Value: string }[];
  PageName?: string;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
  FixedWidth?: string;
  FixedHeight?: string;
  OverflowName?: string;
};
export type Table = {
  Name: string;
  Type: "table";
  KeepTogether?: boolean;
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    LineHeight?: string;
    Format?: string;
    TextDecoration?: string;
    TextAlign?: string;
    VerticalAlign?: string;
    Color?: string;
    Direction?: string;
    WritingMode?: string;
    Language?: string;
    Calendar?: string;
    NumeralLanguage?: string;
    NumeralVariant?: string;
    UnicodeBiDi?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  Filters?: Filter[];
  NewSection?: boolean;
  SortExpressions?: SortExpression[];
  NoRowsMessage?: string;
  DataSetName?: string;
  DataSetParameters?: { [x: string]: any; Name: string; Value: string }[];
  PageName?: string;
  FillPage?: boolean;
  DetailDataElementName?: string;
  DetailDataCollectionName?: string;
  DetailDataElementOutput?: DataElementOutput;
  FrozenRows?: number;
  FrozenColumns?: number;
  RepeatToFill?: boolean;
  PreventOrphanedFooter?: boolean;
  TableColumns?: TableColumn[];
  Header?: TableHeader;
  TableGroups?: TableGroup[];
  Details?: TableDetails;
  Footer?: TableFooter;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
  FixedWidth?: string;
  FixedHeight?: string;
  OverflowName?: string;
};
export type TableColumn = {
  Width?: string;
  FixedHeader?: boolean;
  Visibility?: Visibility;
  AutoWidth?: "None" | "Proportional";
};
export type TableHeader = {
  FixedHeader?: boolean;
  TableRows?: TableRow[];
  RepeatOnNewPage?: boolean;
};
export type TableRow = {
  Height?: string;
  Visibility?: Visibility;
  TableCells?: TableCell[];
};
export type TableFooter = {
  PrintAtBottom?: boolean;
  TableRows?: TableRow[];
  RepeatOnNewPage?: boolean;
};
export type TableDetails = {
  TableRows?: TableRow[];
  Group?: Grouping;
  SortExpressions?: SortExpression[];
  Visibility?: Visibility;
};
export type TableGroup = {
  Group?: Grouping;
  SortExpressions?: SortExpression[];
  Header?: TableHeader;
  Footer?: TableFooter;
  Visibility?: Visibility;
  KeepTogether?: boolean;
  PreventOrphanedFooter?: boolean;
};
export type TableCell = {
  Item: ReportItem;
  ColSpan?: number;
  RowSpan?: number;
  AutoMergeMode?: "Never" | "Always" | "Restricted";
};
export type Tablix = {
  Name: string;
  Type: "tablix";
  KeepTogether?: boolean;
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    LineHeight?: string;
    Format?: string;
    TextDecoration?: string;
    TextAlign?: string;
    VerticalAlign?: string;
    Color?: string;
    Direction?: string;
    WritingMode?: string;
    Language?: string;
    Calendar?: string;
    NumeralLanguage?: string;
    NumeralVariant?: string;
    UnicodeBiDi?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  Filters?: Filter[];
  NewSection?: boolean;
  SortExpressions?: SortExpression[];
  NoRowsMessage?: string;
  DataSetName?: string;
  DataSetParameters?: { [x: string]: any; Name: string; Value: string }[];
  PageName?: string;
  FrozenRows?: number;
  FrozenColumns?: number;
  RepeatColumnHeaders?: boolean;
  RepeatRowHeaders?: boolean;
  GroupsBeforeRowHeaders?: number;
  LayoutDirection?: "LTR" | "RTL";
  RowHierarchy?: TablixHierarchy;
  ColumnHierarchy?: TablixHierarchy;
  Corner?: TablixCornerRow[];
  Body?: TablixBody;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
  FixedWidth?: string;
  FixedHeight?: string;
  OverflowName?: string;
  RepeatToFill?: boolean;
};
export type TablixBody = { Columns?: string[]; Rows?: TablixBodyRow[] };
export type TablixBodyRow = { Height?: string; Cells?: TablixBodyCell[] };
export type TablixBodyCell = {
  AutoMergeMode?: "Never" | "Always" | "Restricted";
  RowSpan?: number;
  ColSpan?: number;
  Item?: ReportItem;
};
export type TablixCornerRow = TablixCornerCell[];
export type TablixCornerCell = {
  RowSpan?: number;
  ColSpan?: number;
  Item?: ReportItem;
};
export type TablixHierarchy = {
  LevelSizes?: string[];
  Members?: TablixMember[];
};
export type TablixMember = {
  Header?: TablixHeader;
  BodyIndex?: number;
  BodyCount?: number;
  Visibility?: Visibility;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  KeepTogether?: boolean;
  Group?: TablixGrouping;
  KeepWithGroup?: "None" | "Before" | "After";
  RepeatOnNewPage?: boolean;
  SortExpressions?: SortExpression[];
} & { Children?: TablixMember[] };
export type TablixHeader = { Item?: ReportItem; LevelCount?: number };
export type TablixGrouping = {
  PrintFooterAtBottom?: boolean;
  Name?: string;
  DocumentMapLabel?: string;
  GroupExpressions?: string[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  PageBreakDisabled?: string;
  Filters?: Filter[];
  ParentGroup?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  DataCollectionName?: string;
  NewSection?: boolean;
};
export type Line = {
  Name: string;
  Type: "line";
  ZIndex?: number;
  Visibility?: Visibility;
  Bookmark?: string;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  StartPoint?: { X?: string; Y?: string };
  EndPoint?: { X?: string; Y?: string };
  LineWidth?: string;
  LineStyle?: string;
  LineColor?: string;
};
export type Rectangle = {
  Name: string;
  Type: "rectangle";
  KeepTogether?: boolean;
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  ConsumeWhiteSpace?: boolean;
  ReportItems?: ReportItem[];
  PageName?: string;
  RoundingRadius?: RoundingRadius;
  LinkToChild?: string;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type RoundingRadius = {
  Default?: string;
  TopLeft?: string;
  TopRight?: string;
  BottomLeft?: string;
  BottomRight?: string;
};
export type BandedList = {
  Name: string;
  Type: "bandedlist";
  KeepTogether?: boolean;
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  Filters?: Filter[];
  NewSection?: boolean;
  ConsumeWhiteSpace?: boolean;
  DataSetName?: string;
  DataSetParameters?: { [x: string]: any; Name: string; Value: string }[];
  PageName?: string;
  PreventOrphanedFooter?: boolean;
  Header?: BandedListHeader;
  Details?: {
    ReportItems?: ReportItem[];
    Name?: string;
    CanShrink?: boolean;
    CanGrow?: boolean;
    KeepTogether?: boolean;
    Height?: string;
    Visibility?: Visibility;
    PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  };
  Footer?: BandedListFooter;
  PreventOrphanedHeader?: boolean;
  Groups?: BandedListGroup[];
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
  FixedWidth?: string;
  FixedHeight?: string;
  OverflowName?: string;
};
export type BandedListHeader = {
  RepeatOnNewPage?: boolean;
  ReportItems?: ReportItem[];
  Name?: string;
  CanShrink?: boolean;
  CanGrow?: boolean;
  KeepTogether?: boolean;
  Height?: string;
  Visibility?: Visibility;
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
};
export type BandedListFooter = {
  RepeatOnNewPage?: boolean;
  PrintAtBottom?: boolean;
  ReportItems?: ReportItem[];
  Name?: string;
  CanShrink?: boolean;
  CanGrow?: boolean;
  KeepTogether?: boolean;
  Height?: string;
  Visibility?: Visibility;
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
};
export type BandedListGroup = {
  Group?: Grouping;
  SortExpressions?: SortExpression[];
  Visibility?: Visibility;
  KeepTogether?: boolean;
  PreventOrphanedHeader?: boolean;
  PreventOrphanedFooter?: boolean;
  Header?: BandedListHeader;
  Footer?: BandedListFooter;
};
export type Subreport = {
  Name: string;
  Type: "subreport";
  KeepTogether?: boolean;
  Style?: {
    LineHeight?: string;
    Format?: string;
    TextDecoration?: string;
    TextAlign?: string;
    VerticalAlign?: string;
    Color?: string;
    Direction?: string;
    WritingMode?: string;
    Language?: string;
    Calendar?: string;
    NumeralLanguage?: string;
    NumeralVariant?: string;
    UnicodeBiDi?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  NoRowsMessage?: string;
  PageName?: string;
  ReportName?: string;
  MergeTransactions?: boolean;
  SubstituteThemeOnSubreport?: boolean;
  InheritStyleSheet?: boolean;
  Parameters?: { ParameterName?: string; Value?: string; Omit?: string }[];
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
  OverflowName?: string;
};
export type Shape = {
  Name: string;
  Type: "shape";
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  RoundingRadius?: RoundingRadius;
  ShapeStyle?: string;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type TableOfContents = {
  Name: string;
  Type: "tableofcontents";
  Style?: {
    BackgroundColor?: string;
    MaxLevel?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  Levels?: TableOfContentsLevel[];
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
  FixedHeight?: string;
  OverflowName?: string;
};
export type TableOfContentsLevel = {
  LevelName?: string;
  DataElementName?: string;
  DisplayFillCharacters?: boolean;
  DisplayPageNumber?: boolean;
  Style?: {
    BackgroundColor?: string;
    TextDecoration?: string;
    TextAlign?: string;
    Color?: string;
    TextIndent?: string;
    LeadingChar?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
  };
};
export type Barcode = {
  Name: string;
  Value?: string;
  Type: "barcode";
  DataElementStyle?: "Auto" | "AttributeNormal" | "ElementNormal";
  Style?: {
    BackgroundColor?: string;
    Format?: string;
    TextDecoration?: string;
    TextAlign?: string;
    Color?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  InvalidBarcodeText?: string;
  Symbology?:
    | "None"
    | "Ansi39"
    | "Ansi39x"
    | "BC412"
    | "Codabar"
    | "Code_11"
    | "Code_128_A"
    | "Code_128_B"
    | "Code_128_C"
    | "Code_128auto"
    | "Code_2_of_5"
    | "Code_93"
    | "Code25intlv"
    | "Code39"
    | "Code39x"
    | "Code49"
    | "Code93x"
    | "DataMatrix"
    | "EAN_13"
    | "EAN_8"
    | "EAN128FNC1"
    | "GS1QRCode"
    | "HIBCCode128"
    | "HIBCCode39"
    | "IATA_2_of_5"
    | "IntelligentMail"
    | "IntelligentMailPackage"
    | "ISBN"
    | "ISMN"
    | "ISSN"
    | "ITF14"
    | "JapanesePostal"
    | "Matrix_2_of_5"
    | "MaxiCode"
    | "MicroPDF417"
    | "MicroQRCode"
    | "MSI"
    | "Pdf417"
    | "Pharmacode"
    | "Plessey"
    | "PostNet"
    | "PZN"
    | "QRCode"
    | "RM4SCC"
    | "RSS14"
    | "RSS14Stacked"
    | "RSS14StackedOmnidirectional"
    | "RSS14Truncated"
    | "RSSExpanded"
    | "RSSExpandedStacked"
    | "RSSLimited"
    | "SSCC_18"
    | "Telepen"
    | "UCCEAN128"
    | "UPC_A"
    | "UPC_E0"
    | "UPC_E1";
  CheckSum?: boolean;
  BarHeight?: string;
  CaptionGrouping?: boolean;
  CaptionLocation?: "None" | "Above" | "Below";
  Code49Options?: BarcodeCode49Options;
  DataMatrixOptions?: BarcodeDataMatrixOptions;
  Ean128Fnc1Options?: BarcodeEan128Fnc1Options;
  Gs1CompositeOptions?: BarcodeGs1CompositeOptions;
  MaxiCodeOptions?: BarcodeMaxiCodeOptions;
  MicroPdf417Options?: BarcodeMicroPdf417Options;
  MicroQrCodeOptions?: BarcodeMicroQrCodeOptions;
  NarrowBarWidth?: string;
  NWRation?: number;
  Pdf417Options?: BarcodePdf417Options;
  QrCodeOptions?: BarcodeQrCodeOptions;
  QuietZone?: { Left?: string; Right?: string; Top?: string; Bottom?: string };
  Rotation?: string;
  RssExpandedStacked?: BarcodeRssExpandedStacked;
  SupplementOptions?: BarcodeSupplementOptions;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type BarcodeCode49Options = { Grouping?: boolean; GroupNumber?: number };
export type BarcodeDataMatrixOptions = {
  EccMode?: "ECC200" | "ECC000" | "ECC050" | "ECC080" | "ECC100" | "ECC140";
  Ecc200SymbolSize?:
    | "SquareAuto"
    | "RectangularAuto"
    | "Square10"
    | "Square12"
    | "Square14"
    | "Square16"
    | "Square18"
    | "Square20"
    | "Square22"
    | "Square24"
    | "Square26"
    | "Square32"
    | "Square36"
    | "Square40"
    | "Square44"
    | "Square48"
    | "Square52"
    | "Square64"
    | "Square72"
    | "Square80"
    | "Square88"
    | "Square96"
    | "Square104"
    | "Square120"
    | "Square132"
    | "Square144"
    | "Rectangular8x18"
    | "Rectangular8x32"
    | "Rectangular12x26"
    | "Rectangular12x36"
    | "Rectangular16x36"
    | "Rectangular16x48";
  Ecc200EncodingMode?:
    | "Auto"
    | "Text"
    | "ASCII"
    | "C40"
    | "X12"
    | "EDIFACT"
    | "Base256";
  Ecc000_140SymbolSize?:
    | "Auto"
    | "Square9"
    | "Square11"
    | "Square13"
    | "Square15"
    | "Square17"
    | "Square19"
    | "Square21"
    | "Square23"
    | "Square25"
    | "Square27"
    | "Square29"
    | "Square31"
    | "Square33"
    | "Square35"
    | "Square37"
    | "Square39"
    | "Square41"
    | "Square43"
    | "Square45"
    | "Square47"
    | "Square49";
  StructuredAppend?: boolean;
  StructureNumber?: number;
  FileIdentifier?: number;
  Encoding?: string;
};
export type BarcodeEan128Fnc1Options = {
  Resolution?: number;
  ModuleSize?: number;
  BarAdjust?: number;
};
export type BarcodeGs1CompositeOptions = {
  CompositeType?: "None" | "CCA";
  Value?: string;
};
export type BarcodeMaxiCodeOptions = {
  Mode?: "Mode4" | "Mode2" | "Mode3" | "Mode5" | "Mode6";
};
export type BarcodeMicroPdf417Options = {
  CompactionMode?:
    | "Auto"
    | "TextCompactionMode"
    | "NumericCompactionMode"
    | "ByteCompactionMode";
  Version?:
    | "ColumnPriorAuto"
    | "RowPriorAuto"
    | "Version1X11"
    | "Version1X14"
    | "Version1X17"
    | "Version1X20"
    | "Version1X24"
    | "Version1X28"
    | "Version2X8"
    | "Version2X11"
    | "Version2X14"
    | "Version2X17"
    | "Version2X20"
    | "Version2X23"
    | "Version2X26"
    | "Version3X6"
    | "Version3X8"
    | "Version3X10"
    | "Version3X12"
    | "Version3X15"
    | "Version3X20"
    | "Version3X26"
    | "Version3X32"
    | "Version3X38"
    | "Version3X44"
    | "Version4X4"
    | "Version4X6"
    | "Version4X8"
    | "Version4X10"
    | "Version4X12"
    | "Version4X15"
    | "Version4X20"
    | "Version4X26"
    | "Version4X32"
    | "Version4X38"
    | "Version4X44";
  SegmentIndex?: number;
  SegmentCount?: number;
  FileID?: number;
};
export type BarcodeMicroQrCodeOptions = {
  Version?: "Auto" | "M1" | "M2" | "M3" | "M4";
  ErrorLevel?: "L" | "M" | "Q";
  Mask?: "Auto" | "Mask00" | "Mask01" | "Mask10" | "Mask11";
  Encoding?: string;
};
export type BarcodePdf417Options = {
  Columns?: number;
  Rows?: number;
  ErrorCorrectionLevel?:
    | "Level0"
    | "Level1"
    | "Level2"
    | "Level3"
    | "Level4"
    | "Level5"
    | "Level6"
    | "Level7"
    | "Level8";
  Pdf417Type?: "Normal" | "Simple";
};
export type BarcodeQrCodeOptions = {
  Connection?: boolean;
  ConnectionNumber?: number;
  Version?: number;
  ErrorLevel?: "L" | "M" | "Q" | "H";
  Mask?:
    | "Auto"
    | "Mask000"
    | "Mask001"
    | "Mask010"
    | "Mask011"
    | "Mask100"
    | "Mask101"
    | "Mask110"
    | "Mask111";
  Model?: "Model2" | "Model1";
  Encoding?: string;
};
export type BarcodeRssExpandedStacked = { RowCount?: number };
export type BarcodeSupplementOptions = {
  Value?: string;
  BarHeight?: string;
  CaptionLocation?: string;
  Spacing?: string;
};
export type DvChart = {
  Name: string;
  Type: "dvchart";
  Style?: {
    BackgroundColor?: string;
    BackgroundGradientEndColor?: string;
    BackgroundGradientType?: string;
    BackgroundImage?: BackgroundImage;
    LineHeight?: string;
    Format?: string;
    TextDecoration?: string;
    TextAlign?: string;
    VerticalAlign?: string;
    Color?: string;
    Direction?: string;
    WritingMode?: string;
    Language?: string;
    Calendar?: string;
    NumeralLanguage?: string;
    NumeralVariant?: string;
    UnicodeBiDi?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  PageBreak?: "None" | "Start" | "End" | "StartAndEnd" | "Between";
  Filters?: Filter[];
  NewSection?: boolean;
  NoRowsMessage?: string;
  DataSetName?: string;
  DataSetParameters?: { [x: string]: any; Name: string; Value: string }[];
  PageName?: string;
  Header?: DvChartHeaderFooter;
  Footer?: DvChartHeaderFooter;
  Bar?: {
    BottomWidth?: number;
    NeckHeight?: number;
    Overlap?: number;
    TopWidth?: number;
    Width?: number;
  };
  Palette?: DvChartPalette;
  CustomPalette?: string[];
  Legend?: DvChartGlobalLegend;
  PlotArea?: DvChartPlotArea;
  Plots?: DvChartPlot[];
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type DvChartGlobalLegend = {
  Wrapping?: boolean;
  Hidden?: boolean;
  Orientation?: "Horizontal" | "Vertical";
  Position?: "Left" | "Right" | "Top" | "Bottom";
  Style?: {
    Border?: BorderStyle;
    BackgroundColor?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
  };
  TextStyle?: DvChartTextStyle;
  TitleStyle?: DvChartTextStyle;
};
export type DvChartPlot = {
  PlotType?:
    | "Bar"
    | "Line"
    | "Area"
    | "Scatter"
    | "HighLowOpenClose"
    | "Candlestick";
  PlotName?: string;
  Encodings?: DvChartEncodings;
  Config?: DvChartPlotConfig;
  PlotChartType?:
    | "Custom"
    | "Bar"
    | "Line"
    | "Area"
    | "Scatter"
    | "HighLowOpenClose"
    | "Candlestick"
    | "Column"
    | "Pie"
    | "Pyramid"
    | "Funnel"
    | "Bubble"
    | "Gantt"
    | "HighLowClose"
    | "PolarColumn"
    | "PolarBar"
    | "RadarArea"
    | "RadarBubble"
    | "RadarScatter"
    | "RadarLine";
  PlotChartSubtype?: "Plain" | "Clustered" | "Stacked";
};
export type DvChartEncodings = {
  Values?: DvChartValueAggregateEncoding[];
  Category?: DvChartCategoryEncoding;
  Details?: DvChartDetailEncoding[];
  Color?: DvChartAggregateEncoding;
  Shape?: DvChartAggregateEncoding;
  Size?: DvChartAggregateEncoding;
};
export type DvChartAggregateEncoding = {
  Field?: { FieldType?: "Simple"; Value?: string[] };
  Aggregate?: DvChartAggregate;
};
export type DvChartCategoryEncoding = {
  Field?: { FieldType?: "Simple"; Value?: string[] };
  Sort?: "None" | "Ascending" | "Descending";
  SortingField?: string;
  SortingAggregate?: DvChartAggregate;
};
export type DvChartValueAggregateEncoding = {
  Field?:
    | { FieldType?: "Complex"; Subfields?: { Key?: string; Value?: string }[] }
    | { FieldType?: "Simple"; Value?: string[] };
  Aggregate?: DvChartAggregate;
};
export type DvChartDetailEncoding = {
  Field?: { FieldType?: "Simple"; Value?: string[] };
  Group?: "None" | "Cluster" | "Stack";
  ExcludeNulls?: boolean;
  Sort?: "None" | "Ascending" | "Descending";
  SortingField?: string;
  SortingAggregate?: DvChartAggregate;
};
export type DvChartAggregate =
  | "None"
  | "Average"
  | "Count"
  | "CountOfAll"
  | "List"
  | "Max"
  | "Min"
  | "PopulationStandardDeviation"
  | "PopulationVariance"
  | "Range"
  | "StandardDeviation"
  | "Sum"
  | "Variance";
export type DvChartPlotConfig = {
  Action?: Action;
  AxisMode?: "Cartesian" | "Radial" | "Polygonal";
  Bar?: {
    BottomWidth?: number;
    NeckHeight?: number;
    Overlap?: number;
    TopWidth?: number;
    Width?: number;
  };
  BarLines?: boolean;
  ClippingMode?: "None" | "Clip" | "Fit";
  InnerRadius?: number;
  LineAspect?: "Default" | "Spline" | "StepCenter" | "StepLeft" | "StepRight";
  LineStyle?: SideStyle;
  Offset?: number;
  Opacity?: number;
  Radial?: boolean;
  ShowNulls?: "Gaps" | "Connected" | "Zeros";
  StartAngle?: number;
  Style?: { BackgroundColor?: string };
  SwapAxes?: boolean;
  Sweep?: number;
  SymbolOpacity?: number;
  Symbols?: boolean;
  SymbolShape?:
    | "Auto"
    | "X"
    | "Dot"
    | "Box"
    | "Diamond"
    | "Triangle"
    | "Dash"
    | "Plus";
  SymbolStyle?: { Border?: BorderStyle; BackgroundColor?: string };
  Text?: {
    ConnectingLine?: { Border?: BorderStyle };
    LinePosition?: "Auto" | "Center";
    Offset?: number;
    OverlappingLabels?: "Auto" | "Show";
    TextPosition?: "Auto" | "Center" | "Inside" | "Outside";
    Template?: string;
    Style?: {
      Border?: BorderStyle;
      BackgroundColor?: string;
      TextDecoration?: string;
      Color?: string;
      FontStyle?: string;
      FontFamily?: string;
      FontSize?: string;
      FontWeight?: string;
    };
  };
  Tooltip?: { Template?: string };
  Rules?: DvChartRule[];
};
export type DvChartRule = {
  Condition?: string;
  RuleProperties?: {
    TargetProperty?:
      | "BackgroundColor"
      | "LineWidth"
      | "LineStyle"
      | "LineColor"
      | "SymbolBackgroundColor"
      | "SymbolLineColor"
      | "SymbolLineStyle"
      | "SymbolLineWidth"
      | "LabelTemplate"
      | "TooltipTemplate";
    Value?: string;
  }[];
};
export type DvChartPlotArea = {
  Axes?: DvChartAxis[];
  Legends?: DvChartLegendOption[];
  Style?: {
    Border?: BorderStyle;
    BackgroundColor?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
  };
};
export type DvChartLegendOption = {
  MaxHeight?: number;
  MaxWidth?: number;
  Title?: string;
  LegendType?: "Color" | "Shape" | "Size";
  IconColor?: string;
  Hidden?: boolean;
  Orientation?: "Horizontal" | "Vertical";
  Position?: "Left" | "Right" | "Top" | "Bottom";
  Style?: {
    Border?: BorderStyle;
    BackgroundColor?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
  };
  TextStyle?: DvChartTextStyle;
  TitleStyle?: DvChartTextStyle;
};
export type DvChartAxis = {
  AxisLine?: boolean;
  AxisType?: "X" | "Y";
  Format?: string;
  LabelAngle?: number;
  Labels?: boolean;
  LabelStyle?: DvChartTextStyleWithPadding;
  LineStyle?: { Border?: BorderStyle };
  LogBase?: number;
  MajorGrid?: boolean;
  MajorGridStyle?: { Border?: BorderStyle };
  MajorTicks?: "None" | "Inside" | "Outside" | "Cross";
  MajorTickSize?: string;
  MajorTickStyle?: { Border?: BorderStyle };
  MajorUnit?: string;
  Max?: string;
  Min?: string;
  MinorGrid?: boolean;
  MinorGridStyle?: { Border?: BorderStyle };
  MinorTicks?: "None" | "Inside" | "Outside" | "Cross";
  MinorTickSize?: string;
  MinorTickStyle?: { Border?: BorderStyle };
  MinorUnit?: string;
  Origin?: string;
  OverlappingLabels?: "Auto" | "Show";
  Plots?: string[];
  Position?: "None" | "Near" | "Far";
  Reversed?: boolean;
  Scale?: "Linear" | "Logarithmic" | "Ordinal" | "Percentage";
  Style?: { Border?: BorderStyle };
  TextStyle?: DvChartTextStyleWithPadding;
  Title?: string;
  TitleStyle?: DvChartTextStyleWithPadding;
};
export type DvChartHeaderFooter = {
  Style?: {
    Border?: BorderStyle;
    BackgroundColor?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
  };
  TextStyle?: DvChartTextStyle;
  Title?: string;
  Height?: string | number;
  HAlign?: "Center" | "Left" | "Right";
  VAlign?: "Top" | "Middle" | "Bottom";
};
export type DvChartPalette =
  | "Light"
  | "Office"
  | "Aspect"
  | "Blue"
  | "Blue2"
  | "BlueGreen"
  | "BlueWarm"
  | "Cerulan"
  | "Cocoa"
  | "Coral"
  | "Cyborg"
  | "Dark"
  | "Darkly"
  | "Flatly"
  | "Grayscale"
  | "Green"
  | "GreenYellow"
  | "HighContrast"
  | "Marquee"
  | "Median"
  | "Midnight"
  | "Modern"
  | "Office2010"
  | "Orange"
  | "OrangeRed"
  | "Organic"
  | "Paper"
  | "Red"
  | "RedOrange"
  | "RedViolet"
  | "Slate"
  | "Slipstream"
  | "Standard"
  | "Superhero"
  | "Violet"
  | "Violet2"
  | "Yellow"
  | "YellowOrange"
  | "Zen"
  | "Custom";
export type DvChartTextStyle = {
  TextDecoration?: string;
  Color?: string;
  FontStyle?: string;
  FontFamily?: string;
  FontSize?: string;
  FontWeight?: string;
};
export type DvChartTextStyleWithPadding = {
  PaddingLeft?: string;
  PaddingRight?: string;
  PaddingTop?: string;
  PaddingBottom?: string;
  TextDecoration?: string;
  Color?: string;
  FontStyle?: string;
  FontFamily?: string;
  FontSize?: string;
  FontWeight?: string;
};
export type Image = {
  Name: string;
  Value?: string;
  Type: "image";
  Action?: Action;
  Style?: {
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  Source?: "External" | "Embedded" | "Database";
  MIMEType?: string;
  Sizing?: "Clip" | "Fit" | "AutoSize" | "FitProportional";
  HorizontalAlignment?: string;
  VerticalAlignment?: string;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type Bullet = {
  Name: string;
  Value?: string;
  Type: "bullet";
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  Orientation?: "Horizontal" | "Vertical";
  LabelFormat?: string;
  LabelFontFamily?: string;
  LabelFontSize?: string;
  LabelFontStyle?: string;
  LabelFontColor?: string;
  ShowLabels?: boolean;
  TargetShape?: "Line" | "Dot" | "Square";
  TargetLineColor?: string;
  TargetLineWidth?: string;
  TickMarks?: "None" | "Inside" | "Outside";
  TicksLineColor?: string;
  TicksLineWidth?: string;
  ValueColor?: string;
  BestValue?: string;
  Interval?: string;
  Range1Boundary?: string;
  Range2Boundary?: string;
  TargetValue?: string;
  WorstValue?: string;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type FormattedText = {
  Name: string;
  Type: "formattedtext";
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  EncodeMailMergeFields?: boolean;
  Html?: string;
  MailMergeFields?: { Name?: string; Value?: string }[];
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type InputField = InputFieldText | InputFieldCheckbox;
export type InputFieldText = {
  Name: string;
  Type: "inputfield";
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  FieldName?: string;
  Readonly?: boolean;
  Required?: boolean;
  Password?: boolean;
  Multiline?: boolean;
  SpellCheck?: boolean;
  MaxLength?: number;
  Value?: string;
  Style?: {
    BackgroundColor?: string;
    Format?: string;
    TextAlign?: string;
    Color?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type InputFieldCheckbox = {
  Name: string;
  Type: "inputfield";
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  FieldName?: string;
  Readonly?: boolean;
  Required?: boolean;
  InputType?: "CheckBox";
  Checked?: string;
  CheckStyle?: "Cross" | "Diamond" | "Square" | "Check" | "Circle" | "Star";
  CheckSize?: string;
  Style?: {
    BackgroundColor?: string;
    TextAlign?: string;
    Color?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type Sparkline = {
  Name: string;
  Type: "sparkline";
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  LineWidth?: string;
  LineColor?: string;
  Filters?: Filter[];
  SortExpressions?: SortExpression[];
  Group?: Grouping;
  DataSetName?: string;
  DataSetParameters?: { [x: string]: any; Name: string; Value: string }[];
  FillColor?: string;
  GradientStyle?: SparklineGradientStyle;
  GradientEndColor?: string;
  MarkerColor?: string;
  MarkerVisibility?: boolean;
  MaximumColumnWidth?: string;
  RangeFillColor?: string;
  RangeGradientStyle?: SparklineGradientStyle;
  RangeGradientEndColor?: string;
  RangeLowerBound?: string;
  RangeUpperBound?: string;
  RangeVisibility?: boolean;
  SparklineType?: "Columns" | "Line" | "Area" | "Whiskers" | "StackedBar";
  SeriesValue?: string;
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type SparklineGradientStyle =
  | "None"
  | "LeftRight"
  | "TopBottom"
  | "Center"
  | "DiagonalLeft"
  | "DiagonalRight"
  | "HorizontalCenter"
  | "VerticalCenter";
export type OverflowPlaceholder = {
  Name: string;
  Left?: string;
  Top?: string;
  Type: "overflowplaceholder";
  LayerName?: string;
  Width?: string;
  Height?: string;
  OverflowName?: string;
};
export type Richtext = {
  Name: string;
  Type: "richtext";
  CanGrow?: boolean;
  DataElementStyle?: "Auto" | "AttributeNormal" | "ElementNormal";
  KeepTogether?: boolean;
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    LineHeight?: string;
    Color?: string;
    PaddingLeft?: string;
    PaddingRight?: string;
    PaddingTop?: string;
    PaddingBottom?: string;
    FontStyle?: string;
    FontFamily?: string;
    FontSize?: string;
    FontWeight?: string;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ZIndex?: number;
  Visibility?: Visibility;
  ToolTip?: string;
  Bookmark?: string;
  DataElementName?: string;
  DataElementOutput?: DataElementOutput;
  Label?: string;
  LayerName?: string;
  CustomProperties?: { Name: string; Value: string }[];
  MarkupType?: "HTML";
  Left?: string;
  Top?: string;
  Width?: string;
  Height?: string;
};
export type ReportItemType =
  | "textbox"
  | "checkbox"
  | "line"
  | "list"
  | "table"
  | "tablix"
  | "bandedlist"
  | "rectangle"
  | "subreport"
  | "shape"
  | "tableofcontents"
  | "barcode"
  | "dvchart"
  | "image"
  | "bullet"
  | "formattedtext"
  | "inputfield"
  | "sparkline"
  | "richtext"
  | "unknown";
export type ReportItem =
  | Textbox
  | Checkbox
  | Line
  | List
  | Table
  | Tablix
  | BandedList
  | Rectangle
  | Subreport
  | Shape
  | TableOfContents
  | Barcode
  | DvChart
  | Image
  | Bullet
  | FormattedText
  | InputFieldText
  | InputFieldCheckbox
  | OverflowPlaceholder
  | Sparkline
  | Richtext;
export type Report = FplReport | CplReport;
export type CplReport = {
  PageHeader?: PageSection;
  Body?: Body;
  PageFooter?: PageSection;
  Page?: Page;
  DataSources?: DataSource[];
  LocalizationResources?: LocalizationResource[];
  Name?: string;
  Description?: string;
  Author?: string;
  Width?: string;
  Language?: string;
  ConsumeContainerWhitespace?: boolean;
  DataElementName?: string;
  DataElementStyle?: "Auto" | "AttributeNormal" | "ElementNormal";
  DocumentMap?: DocumentMap;
  ReportParameters?: ReportParameter[];
  EmbeddedImages?: EmbeddedImage[];
  StartPageNumber?: number;
  Layers?: { Name: string }[];
  Themes?: string[];
  ThemeUri?: string;
  Code?: string;
  CustomProperties?: { Name: string; Value: string }[];
  DataSets?: DataSet[];
};
export type FplReport = {
  FixedPage?: {
    Pages?: FixedPageSection[];
    Group?: Grouping;
    DataSetName?: string;
    Filters?: Filter[];
    SortExpressions?: SortExpression[];
    DataElementName?: string;
    DataElementOutput?: DataElementOutput;
  };
  CollateBy?: "Value" | "Simple" | "ValueIndex";
  Page?: Page;
  DataSources?: DataSource[];
  LocalizationResources?: LocalizationResource[];
  Name?: string;
  Description?: string;
  Author?: string;
  Width?: string;
  Language?: string;
  ConsumeContainerWhitespace?: boolean;
  DataElementName?: string;
  DataElementStyle?: "Auto" | "AttributeNormal" | "ElementNormal";
  DocumentMap?: DocumentMap;
  ReportParameters?: ReportParameter[];
  EmbeddedImages?: EmbeddedImage[];
  StartPageNumber?: number;
  Layers?: { Name: string }[];
  Themes?: string[];
  ThemeUri?: string;
  Code?: string;
  CustomProperties?: { Name: string; Value: string }[];
  DataSets?: DataSet[];
};
export type LocalizationResource = {
  Language?: string;
  Data?: { Name?: string; Value?: string }[];
};
export type Page = {
  PageWidth?: string;
  PageHeight?: string;
  RightMargin?: string;
  LeftMargin?: string;
  TopMargin?: string;
  BottomMargin?: string;
  Columns?: number;
  ColumnSpacing?: string;
};
export type DocumentMap = {
  Source?: "None" | "Labels" | "Headings" | "All";
  NumberingStyle?:
    | "None"
    | "Decimal"
    | "DecimalLeadingZero"
    | "LowerRoman"
    | "UpperRoman"
    | "LowerLatin"
    | "UpperLatin"
    | "CircledNumber"
    | "KatakanaBrackets"
    | "KatakanaIrohaBrackets"
    | "Katakana"
    | "KatakanaIroha"
    | "KatakanaLower"
    | "KatakanaIrohaLower"
    | "LowerGreek"
    | "UpperGreek"
    | "LowerArmenian"
    | "UpperArmenian"
    | "Georgian";
  Levels?: (
    | "None"
    | "Decimal"
    | "DecimalLeadingZero"
    | "LowerRoman"
    | "UpperRoman"
    | "LowerLatin"
    | "UpperLatin"
    | "CircledNumber"
    | "KatakanaBrackets"
    | "KatakanaIrohaBrackets"
    | "Katakana"
    | "KatakanaIroha"
    | "KatakanaLower"
    | "KatakanaIrohaLower"
    | "LowerGreek"
    | "UpperGreek"
    | "LowerArmenian"
    | "UpperArmenian"
    | "Georgian"
  )[];
};
export type ReportParameter = {
  Name: string;
  DataType: "String" | "Boolean" | "Date" | "DateTime" | "Integer" | "Float";
  Hidden?: boolean;
  Multiline?: boolean;
  AllowBlank?: boolean;
  DefaultValue?: DefaultValue;
  MultiValue?: boolean;
  Nullable?: boolean;
  Prompt?: string;
  SelectAllValue?: string;
  UsedInQuery?: ThreeStateBoolean;
  ValidValues?: ValidValues;
};
export type ValidValues = {
  ParameterValues?: ParameterValue[];
  DataSetReference?: DataSetReference;
};
export type DataSetReference = {
  DataSetName: string;
  ValueField: string;
  LabelField?: string;
};
export type ParameterValue = { Value?: string; Label?: string };
export type DefaultValue = {
  Values?: string[];
  DataSetReference?: DataSetReference;
};
export type DataSet = {
  Name: string;
  Filters?: Filter[];
  Fields?: Field[];
  Query?: Query;
  CaseSensitivity?: ThreeStateBoolean;
  Collation?: string;
  KanatypeSensitivity?: ThreeStateBoolean;
  AccentSensitivity?: ThreeStateBoolean;
  WidthSensitivity?: ThreeStateBoolean;
};
export type Field = { Name: string; Value?: string; DataField?: string };
export type Query = {
  DataSourceName: string;
  CommandType?: "Text" | "StoredProcedure" | "TableDirect";
  CommandText?: string;
  QueryParameters?: { [x: string]: any; Name: string; Value: string }[];
  Timeout?: number;
};
export type DataSource = {
  Name: string;
  Transaction?: boolean;
  DataSourceReference?: string;
  ConnectionProperties?: ConnectionProperties;
  $id?: string;
};
export type ConnectionProperties = {
  DataProvider: string;
  Prompt?: string;
  ConnectString?: string;
  IntegratedSecurity?: boolean;
};
export type PageSection = {
  Name?: string;
  Height?: string;
  ReportItems?: ReportItem[];
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  PrintOnFirstPage?: boolean;
  PrintOnLastPage?: boolean;
};
export type Body = {
  Name?: string;
  Height?: string;
  ReportItems?: ReportItem[];
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
};
export type EmbeddedImage = {
  Name?: string;
  ImageData?: string;
  MIMEType?: string;
};
export type FixedPageSection = {
  ReportItems?: ReportItem[];
  Style?: {
    BackgroundColor?: string;
    BackgroundImage?: BackgroundImage;
    Border?: BorderStyle;
    TopBorder?: SideStyle;
    RightBorder?: SideStyle;
    BottomBorder?: SideStyle;
    LeftBorder?: SideStyle;
  };
  ThrowIfPlaceHoldersEmpty?: boolean;
};
